#!/usr/bin/env node
const { Async } = await import('hyphae')
const { default: Parser } = await import('tree-sitter')
const { default: Json } = await import('tree-sitter-json')
const { parseArgs } = await import('node:util')

const { values: parsedArgs } = parseArgs({
  args: process.argv.slice(2),
  options: {
    requires: {
      type: 'string',
      short: 'r',
      multiple: true
    },
    functions: {
      type: 'string',
      short: 'f',
      multiple: true
    }
  }
})

if (parsedArgs.requires) {
  const r = {}
  for (let moduleName of parsedArgs.requires)
    r[moduleName] = require(moduleName)
}

const parser = new Parser()
parser.setLanguage(Json)

const makeJson = (node, object = null) => {
  switch (node.type) {
    case 'document':
      return makeJson(node.child(0))
    case 'array':
      return [
        ...node.children
          .map(childNode => makeJson(childNode))
          .filter(e => e !== undefined)
      ]
    case 'object':
      const json = {}
      node.children.forEach(childNode => makeJson(childNode, json))
      return json
    case 'pair':
      if (object !== null) {
        const key = node.child(0).text.slice(1, -1)
        switch (node.child(2).type) {
          case 'string':
            object[key] = node.child(2).text.slice(1, -1)
            break
          case 'number':
            object[key] = parseFloat(node.child(2).text)
            break
          case 'true':
            object[key] = true
            break
          case 'false':
            object[key] = false
            break
        }
      }
  }
}

let stream = Async.fromReadable(process.stdin)
  // Parse the JSON
  .map(e => e.toString('utf8'))
  .scan(
    (acc, e) => {
      const jsonString = acc.jsonString + e
      return {
        // Use the cached tree
        tree: parser.parse(jsonString, acc.tree),
        jsonString
      }
    },
    { tree: null, jsonString: '' }
  )
  // Streams are transducers, so we can avoid unnecessary computation by filtering here
  .map(e => e.tree.rootNode)
  .last()
  .map(makeJson)
  // Create a stream of entries based on whether it's a JSON array or object
  .map(json => {
    if (json instanceof Array) return Async.fromIterable(json)
    else return Async.fromIterable(Object.entries(json))
  })
  .flatten()

// Transform the JSON stream entries with the functions passed in by the user
if (parsedArgs.functions) {
  for (let fnBody of parsedArgs.functions) {
    const fn = new Function(
      'stream',
      `return stream.${fnBody[0] === '.' ? fnBody.slice(1) : fnBody}`
    )
    stream = fn(stream)
  }
}

stream = stream
  // Collect the results...
  .scan((acc, e) => {
    if (!acc) {
      acc = e instanceof Array && e.length === 2 ? {} : []
    }
    if (acc instanceof Array) acc.push(e)
    else acc[e[0]] = e[1]
    return acc
  }, null)
  .last()
  // ...and then print it to stdout
  .map(JSON.stringify)
  .toWritable(process.stdout)

/*
Example input:

$ echo '{ "hello": "world", "foo": 123, "bar": true }' | node index.js
{"hello":"world!","foo":123,"bar":true}

$ echo '[{ "hello": "world" }, { "foo": 123, "bar": true }]' | node index.js
[{"hello":"world","norf":"blah"},{"foo":123,"bar":true,"norf":"blah"}]
*/
