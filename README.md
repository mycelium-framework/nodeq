# nodeq

A `jq`-like library that uses NodeJS and [Hyphae](https://gitlab.com/mycelium-framework/hyphae) streams to process JSON

## Install

```sh
npm i -g git@gitlab.com:mycelium-framework/nodeq.git
```

## Examples

### JSON Arrays

```sh
$ echo '[{ "sensor": 10 }, { "sensor": 20 }, { "sensor": 30 }]' \
    | nodeq -f ".map(({sensor}) => ({ sensor: sensor * 2 }))" \
    | cat
[{"sensor":20},{"sensor":40},{"sensor":60}]
```

### JSON Objects

```sh
$ echo '{ "hello": "world", "foo": 123, "bar": true }' \
    | nodeq -f \
      ".map(([key, value]) => [key, value + '!'])\
       .filter(([key, value]) => key !== 'foo')" \
    | cat
{"hello":"world!","bar":"true!"}
```


## License

See [LICENSE](./LICENSE)
