- Move away from treesitter for parsing, and instead just create a stack-based
  parser using hyphae streams, for more efficiency
