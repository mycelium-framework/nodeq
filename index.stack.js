// WIP
const { Async } = await import('hyphae')

Async.fromIterable(process.stdin).scan(
  (acc, buffer) => {
    for (let charCode of buffer) {
      const charCode = String.charCodeAt(char)
      if (charCode === '[') {
        acc.stack.push([])
        if (acc.json) acc.json.push(acc.stack[acc.stack - 1])
        else acc.json = acc.stack[acc.stack - 1]
      } else if (charCode === ']') acc.buffers.pop()
    }
    return acc
  },
  { stack: [], json: null }
)
